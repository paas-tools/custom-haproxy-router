---
kind: "Template"
apiVersion: "v1"
metadata:
  name: "router"
  annotations:
    description: "Template for a customized Openshift router for use at CERN"
parameters:
-
  name: "ROUTER_IMAGE"
  description: "Docker image to use for the default router"
  value: "gitlab-registry.cern.ch/paas-tools/origin/origin-haproxy:v3.11.0-cern.4" # Use custom image with CERN patches. See CIPAAS-435
-
  name: "REPLICA_COUNT"
  description: "Number of router replicas"
  required: true
-
  name: ROUTER_CANONICAL_HOSTNAME
  description: "Provides users with the CNAME target to use when registering custom DNS names for their apps. Currently the DNSLB alias FQDN."

objects:
- apiVersion: v1
  kind: DeploymentConfig
  metadata:
    labels:
      openshift.cern.ch/router: router
    name: router
  spec:
    replicas: ${REPLICA_COUNT}
    selector:
      openshift.cern.ch/router: router
    strategy:
      resources: {}
      activeDeadlineSeconds: 21600
      rollingParams:
        intervalSeconds: 1
        maxSurge: 0
        maxUnavailable: 25%
        timeoutSeconds: 3600
        updatePeriodSeconds: 1
      type: Rolling
    template:
      metadata:
        labels:
          openshift.cern.ch/router: router
      spec:
        containers:
        - env:
          - name: DEFAULT_CERTIFICATE_DIR
            value: /etc/pki/tls/private
          - name: ROUTER_CIPHERS
          - name: ROUTER_EXTERNAL_HOST_HOSTNAME
          - name: ROUTER_EXTERNAL_HOST_HTTPS_VSERVER
          - name: ROUTER_EXTERNAL_HOST_HTTP_VSERVER
          - name: ROUTER_EXTERNAL_HOST_INSECURE
            value: "false"
          - name: ROUTER_EXTERNAL_HOST_INTERNAL_ADDRESS
          - name: ROUTER_EXTERNAL_HOST_PARTITION_PATH
          - name: ROUTER_EXTERNAL_HOST_PASSWORD
          - name: ROUTER_EXTERNAL_HOST_PRIVKEY
            value: /etc/secret-volume/router.pem
          - name: ROUTER_EXTERNAL_HOST_USERNAME
          - name: ROUTER_EXTERNAL_HOST_VXLAN_GW_CIDR
          # ROUTER_LISTEN_ADDR replaces STATS_PORT https://github.com/openshift/origin/pull/14790#issuecomment-309941864
          - name: ROUTER_LISTEN_ADDR
            value: 0.0.0.0:1936
          - name: ROUTER_METRICS_TYPE
            value: haproxy
          - name: ROUTER_SERVICE_HTTPS_PORT
            value: "443"
          - name: ROUTER_SERVICE_HTTP_PORT
            value: "80"
          - name: ROUTER_SERVICE_NAME
            value: router
          - name: ROUTER_SERVICE_NAMESPACE
            value: default
          - name: ROUTER_SUBDOMAIN
          - name: STATS_PASSWORD
            valueFrom:
              configMapKeyRef:
                name: customrouter
                key: STATS_PASSWORD
          - name: STATS_USERNAME
            value: admin
          - name: CERN_INTRANET_NETWORKS
            value: 2001:1458::/32 2001:1459::/32 FD01:1458::/32 FD01:1459::/32 10.0.0.0/8
              100.64.0.0/10 128.141.0.0/16 128.142.0.0/16 137.138.0.0/16 172.16.0.0/12
              188.184.0.0/15 192.16.155.0/24 192.16.165.0/24 192.91.242.0/24 192.168.0.0/16
              194.12.128.0/18
          - name: CERN_TECHNICAL_NETWORKS
            value: 172.18.0.0/16
          - name: ROUTER_SYSLOG_ADDRESS
            # well-known IP from https://gitlab.cern.ch/paas-tools/paas-infra/haproxy-logging
            value: "172.30.22.141"
          - name: ROUTER_LOG_LEVEL
            # use "info info" to have ONLY HTTP logs, and not other messages like backend status
            value: info
          - name: ROUTE_LABELS
            value: router.cern.ch/exclude!=true
          - name: ROUTER_DEFAULT_CLIENT_TIMEOUT
            value: 60s
          - name: ROUTER_SLOWLORIS_TIMEOUT
            value: 60s
          - name: ROUTER_BIND_PORTS_AFTER_SYNC
            value: "true"
          - name: ROUTER_IP_V4_V6_MODE
            value: "v4v6"
          - name: TEMPLATE_FILE
            value: /var/lib/haproxy/conf/custom/haproxy-config.template
          - name: ROUTER_CANONICAL_HOSTNAME
            value: ${ROUTER_CANONICAL_HOSTNAME}
          # Certificates for the metrics. CIPAAS-436
          - name: ROUTER_METRICS_TLS_CERT_FILE
            value: /etc/router-metrics/tls.crt
          - name: ROUTER_METRICS_TLS_KEY_FILE
            value: /etc/router-metrics/tls.key
          # OpenShift 3.11 Implements HAProxy router HTTP/2 support but there are some issues:
          # Oc CLI failure on reencrypt route: https://gitlab.cern.ch/vcs/oo-gitlab-cern/-/jobs/3085694 (caused by https://discourse.haproxy.org/t/h2-option-httpclose-error-in-the-http2-framing-layer/1739 ?)
          # Upstream issues? https://github.com/openshift/origin/issues/13638#issuecomment-447564682 
          # TODO: give it another try in a future version (CIPAAS-369)
          - name: ROUTER_ENABLE_HTTP2
            value: "false"
          # OpenShift 3.11 Implements changes to the HAProxy router without requiring a full router reload.
          - name: ROUTER_HAPROXY_CONFIG_MANAGER
            value: "true"
          # CERN patch, it will come from upstream in OpenShift 4.0, add possibility to configure a list of custom annotations to vary HAProxy configuration. https://github.com/openshift/origin/issues/21170
          - name: ROUTER_BLUEPRINT_CUSTOM_ANNOTATIONS
            value: "router.cern.ch/use-dedicated-port,router.cern.ch/network-visibility,router.cern.ch/technical-network-access"
          image: ${ROUTER_IMAGE}
          imagePullPolicy: IfNotPresent
          lifecycle:
            # use lifecycle commands for DNSLB integration: get ourselves out of the DNSLB alias and wait some time for existing connections to terminate
            postStart:
              exec:
                command:
                - bash
                - -c
                - |-
                  # clean previous shutting-down state
                  rm -f /var/run/router-dnslb-integration/shutting_down
            preStop:
              exec:
                command:
                - bash
                - -c
                - |-
                  # indicate to host's DNSLB health script that we're shutting down and we should be out of the DNS alias (cf. CIPAAS-347)
                  # and let any user remove it (in case we change router pod UID)
                  (umask 000 && touch /var/run/router-dnslb-integration/shutting_down)
                  # wait for DNSLB to update
                  sleep 15m
                  WAIT_FOR_EXISTING_CONNECTIONS_DEADLINE=$[ $SECONDS + 30 * 60 ] #wait up to 30 minutes for connections to go away
                  while [[ $SECONDS -lt $WAIT_FOR_EXISTING_CONNECTIONS_DEADLINE && $(echo 'show info' | socat - UNIX-CONNECT:/var/lib/haproxy/run/haproxy.sock | grep -Po '(?<=CurrConns: ).+' ) -gt 0 ]];
                    do sleep 10s;
                  done
          livenessProbe:
            httpGet:
              host: localhost
              path: /healthz
              port: 1936
            # should be 10s, but router 3.6.x stalls for a long time at startup and we must make sure it's not considered unhealthy
            # because of that
            # As of November 2017, in DEV it takes a bit more than 30s:
            # I1115 15:29:32.238057       1 template.go:246] Starting template router (v3.6.1+008f2d5)
            # [...]
            # I1115 15:30:04.939981       1 router.go:554] Router reloaded
            # And in PROD more than 2min:
            # I1116 09:27:40.171270       1 template.go:246] Starting template router (v3.6.1+008f2d5)
            # [...]
            # I1116 09:29:09.615650       1 router.go:554] Router reloaded
            initialDelaySeconds: 300
          name: router
          ports:
          - containerPort: 80
          - containerPort: 443
          - containerPort: 1936
            name: stats
            protocol: TCP
          readinessProbe:
            httpGet:
              host: localhost
              path: /healthz
              port: 1936
            initialDelaySeconds: 10
          resources:
            # CIPAAS-324: on a m2.xlarge (~14GiB), 1GiB is system reserved, allocate 12 for haproxy
            # (the rest goes to daemonsets)
            requests:
              cpu: 2
              memory: 12Gi
            limits:
              cpu: 7
              memory: 12Gi
          volumeMounts:
          - mountPath: /etc/pki/tls/private
            name: server-certificate
            readOnly: true
          - mountPath: /var/lib/haproxy/conf/custom
            name: config-volume
          - mountPath: /var/run/router-dnslb-integration
            name: dnslb-health
          # Certificates for the metrics. CIPAAS-436
          - mountPath: /etc/router-metrics
            name: router-metrics-cert
        hostNetwork: true
        nodeSelector:
          role: router
        securityContext: {}
        serviceAccount: router
        serviceAccountName: router
        terminationGracePeriodSeconds: 3000
        volumes:
        - name: server-certificate
          secret:
            secretName: router-certs
        - name: config-volume
          configMap:
            name: customrouter
        # a place to put information to tell the DNSLB health script on the host when we're shutting down
        - name: dnslb-health
          hostPath:
            path: /var/run/router-dnslb-integration
        # Certificates for the metrics. CIPAAS-436
        - name: router-metrics-cert
          secret:
            secretName: router-metrics-cert
    test: false
    triggers:
    - type: ConfigChange
